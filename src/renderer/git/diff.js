const exec = require('./exec');

// call the function
const diff = () {
  return (projectPath) {
    exec('git diff --name-only --diff-filter=U', (err, stdOut, stdErr) {
      console.log('err', err);
      console.log('stdOut', stdOut);
      console.log('stdErr', stdErr);
    }
  }
}

export default diff
