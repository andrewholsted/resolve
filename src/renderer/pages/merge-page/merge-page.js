import Tophat from '../../components/tophat/main'
import Pane from '../../components/pane/main'
import Sidebar from '../../components/sidebar/main'
import Shoes from '../../components/shoes/main'
import Take from '../../components/take/main'

export default {
  name: 'merge-page',
  components: {
    Tophat,
    Pane,
    Sidebar,
    Shoes,
    Take
  }
}
