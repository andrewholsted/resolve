import Vue from 'vue'

import app from './app/main'
import store from './store'
import {ipcRenderer} from 'electron'

if (!process.env.IS_WEB) {
  Vue.use(require('vue-electron'))
}

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  components: { app },
  store,
  template: '<app/>'
}).$mount('#app')

ipcRenderer.on('application:open', () => {
})
