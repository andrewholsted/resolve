import {Menu} from 'electron'
import resolve from './resolve'

const darwin = require('../../menus/darwin.json')

class ApplicationMenu {
  constructor () {
    this.setTemplate()
  }

  setTemplate () {
    const template = this.transformTemplate(darwin.menu)
    const applicationMenu = Menu.buildFromTemplate(template)
    Menu.setApplicationMenu(applicationMenu)
  }

  transformTemplate (template) {
    template.forEach((item) => {
      if (item.command) {
        item.click = () => resolve.sendCommand(item.command)
      }

      if (item.submenu) {
        return this.transformTemplate(item.submenu)
      }
    })

    return template
  }
}

export default ApplicationMenu
