import electron from 'electron'
import settings from 'electron-settings'
import ApplicationMenu from './application-menu'
import config from '../config'

const { BrowserWindow } = electron

class Resolve {
  constructor () {
    this.mainWindow = null

    if (config.env === 'development') {
      this.windowURL = 'http://localhost:9080'
    } else {
      this.windowURL = `file://${__dirname}/index.html`
    }
  }

  open () {
    const windowSettings = settings.get('window')

    if (windowSettings) {
      this.mainWindow = new BrowserWindow({
        x: windowSettings.x,
        y: windowSettings.y,
        width: windowSettings.width,
        height: windowSettings.height
      })

      if (windowSettings.maximized) {
        this.mainWindow.maximize()
      }
    } else {
      const {width, height} = electron.screen.getPrimaryDisplay().workAreaSize
      this.mainWindow = new BrowserWindow({
        height: height,
        width: width
      })
    }

    this.mainWindow.loadURL(this.windowURL)
    this.applicationMenu = new ApplicationMenu()

    this.mainWindow.on('close', () => {
      const bounds = this.mainWindow.getBounds()
      settings.set('window', {
        x: bounds.x,
        y: bounds.y,
        width: bounds.width,
        height: bounds.height,
        maximized: this.mainWindow.isMaximized()
      })
    })

    this.mainWindow.on('closed', () => {
      this.mainWindow = null
    })
  }

  activate () {
    if (this.mainWindow === null) {
      this.open()
    }
  }

  sendCommand (command) {
    this.mainWindow.send(command)
  }
}

export default new Resolve()
